#pragma once

#include <vector>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"

/*
Animated missile bullet 
Player can only fire one and has to wait for it to leave the 
screen before firing again.
*/
class Bullet
{
public:
	Bullet(MyD3D& d3d);
	bool active = false;
	Sprite spr;

	void Render(DirectX::SpriteBatch& batch);
	void Update(float dTime);
	const float MISSILE_SPEED = 300;
};

class PlayMode;

class Player
{
public:
	Player(PlayMode& pmode);
	void Render(DirectX::SpriteBatch& batch);
	void Update(float dTime);
	const float SPEED = 250;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;

private:
	PlayMode& mPMode;
	Sprite mThrust;		//flames out the back
	Bullet mMissile;	//weapon, only one at once
	Sprite mShip;		//jet
	//once we start thrusting we have to keep doing it for 
	//at least a fraction of a second or it looks whack
	float mThrusting = 0;

	void HandleInput(float dTime);
};

class Game;

//horizontal scrolling with player controlled ship
class PlayMode
{
public:
	PlayMode(Game& myGame);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch);
	Game& GetGame() { return myGame; }
	RECTF& GetPlayArea() { return mPlayArea; }
private:
	const float SCROLL_SPEED = 10.f;
	static const int BGND_LAYERS = 8;

	Game& myGame;
	std::vector<Sprite> mBgnd; //parallax layers
	RECTF mPlayArea;	//don't go outside this	
	Player mPlayer;

	//setup once
	void InitBgnd();

	//make it scroll parallax
	void UpdateBgnd(float dTime);
};


/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { PLAY };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::PLAY;
	Game(MyD3D& d3d);



	void Release();
	void Update(float dTime);
	void Render(float dTime);
	MyD3D& GetD3D() { return mD3D; }
private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
	//not much of a game, but this is it
	PlayMode mPMode;
};


